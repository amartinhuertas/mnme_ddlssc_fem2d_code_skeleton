#################################################################
# Search F90 files recursively in all subdirs
#################################################################

FILE(GLOB_RECURSE LIB_SRC *.f90 *.F90)
SET(LIB_SRC ${LIB_SRC} PARENT_SCOPE)

#################################################################
# Library target
#################################################################

ADD_LIBRARY(${PROJECT_NAME} STATIC ${LIB_SRC})
TARGET_LINK_LIBRARIES(${PROJECT_NAME} ${MPI_Fortran_LIBRARIES})

# The following command just applies for the case of dynamic libraries
# it doesn't make sense in this scenario as we are generating an static library.
# we leaved it here just for illustration purposes.
SET_TARGET_PROPERTIES(${PROJECT_NAME} PROPERTIES VERSION ${${PROJECT_NAME}_VERSION} SOVERSION ${${PROJECT_NAME}_SOVERSION})
