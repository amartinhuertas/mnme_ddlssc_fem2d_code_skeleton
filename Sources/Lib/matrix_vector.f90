module matrix_vector
  use matrix_class
  use vector_class
  implicit none
contains
  subroutine matvec ( A, x, y )
    implicit none
    ! Parameters 
    type(matrix), intent(in)    :: A 
    type(vector), intent(in)    :: x
    type(vector), intent(inout) :: y

    ! Locals
    integer              :: ierr

    ! *** Master on Numerical Methods in Engineering (MNME) ***
    ! *** Module Name: Domain Decomposition and Large Scale Scientific Computing (DDLSSC) ***
    ! *** TASK #5
    ! *** CODE DEVELOPMENT REQUIRED ***
    !     THE BODY OF THE FOLLOWING 
    !     SUBROUTINE MUST BE DEVELOPED
    !     FOR THE FINAL ASSIGNMENT
    if (x%desc%me == 0) then
       write (*,'(a)') '! *** Master on Numerical Methods in Engineering (MNME) ***'
       write (*,'(a)') '! *** Module Name: Domain Decomposition and Large Scale Scientific Computing (DDLSSC) ***'
       write (*,'(a)') '! *** TASK #5'
       write (*,'(a)') '! *** CODE DEVELOPMENT REQUIRED ***'
       write (*,'(a)') '!     THE BODY OF THE FOLLOWING'
       write (*,'(a)') '!     SUBROUTINE MUST BE DEVELOPED'
       write (*,'(a)') '!     FOR THE FINAL ASSIGNMENT'
    end if
    call mpi_barrier(x%desc%mpi_comm, ierr)
    call mpi_abort(A%desc%mpi_comm, -1, ierr)

  end subroutine matvec


end module matrix_vector
