module timer_class
#ifdef MPI_MOD
  use mpi
#endif
  implicit none
#ifdef MPI_H
  include 'mpif.h'
#endif

  type timer
      integer                   :: mpi_comm = mpi_comm_null ! MPI Communicator
      integer                   :: root     = -1            ! PID responsible of gathering/reporting timings
      character(:), allocatable :: message                  ! Concept being measured (e.g., assembly)
      real(8)                   :: start    = 0.0           ! last call to start
      real(8)                   :: stop     = 0.0           ! last call to stop
      real(8)                   :: accum    = 0.0           ! sum of all stop-start 
  end type timer

contains
  
    subroutine timer_create ( message, mpi_comm, t, root )
#ifdef MPI_MOD
      use mpi
#endif
      implicit none 
#ifdef MPI_H
      include 'mpif.h'
#endif
      ! Parameters
      character(*)             , intent(in)    :: message
      integer                  , intent(in)    :: mpi_comm
      type(timer)              , intent(inout) :: t
      integer        , optional, intent(in)    :: root
      
      ! Locals
      integer :: root_
      
      call timer_destroy(t)
      
      root_  = 0 
      if ( present(root)  ) root_  = root 
      t%mpi_comm   = mpi_comm
      t%root       = root_ 
      t%message    = message
      t%start      = 0.0 
      t%stop       = 0.0
      t%accum      = 0.0
    end subroutine timer_create

    subroutine timer_init ( t )
      implicit none 
      ! Parameters
      type(timer), intent(inout)          :: t
      t%start  = 0.0 
      t%stop   = 0.0
      t%accum  = 0.0
    end subroutine timer_init

    subroutine timer_start ( t )
      implicit none 
      ! Parameters
      type(timer), intent(inout)          :: t
      ! Locals
      integer :: ierr
      call mpi_barrier (t%mpi_comm, ierr)
      t%start  = mpi_wtime()
    end subroutine timer_start

    subroutine timer_stop ( t )
      implicit none 
      ! Parameters
      type(timer), intent(inout)          :: t
      t%stop = mpi_wtime()
      if ( t%stop - t%start >= 0.0) then
         t%accum = t%accum + (t%stop - t%start)
      end if
      t%start  = 0.0 
      t%stop   = 0.0
    end subroutine timer_stop
   
    subroutine timer_report ( t, header )
      implicit none 
      ! Parameters
      type(timer)          , intent(inout) :: t
      logical    , optional,  intent(in)   :: header 
      
      ! Locals
      character(len=*), parameter    :: fmt_header = '(a25,1x,3(2x,a15),3(2x,a15))'
      character(len=*), parameter    :: fmt_data   = '(a25,1x,3(2x,es15.9),3(2x,es15.9))'
      real(8)                        :: accum_max, accum_min, accum_sum
      integer                        :: my_id, num_procs, ierr
      logical                        :: header_

      call mpi_comm_size ( t%mpi_comm, num_procs, ierr )
      call mpi_comm_rank ( t%mpi_comm, my_id    , ierr )

      accum_max = t%accum
      accum_min = t%accum
      accum_sum = t%accum
      header_ = .true.
      if (present(header)) header_ = header 

      if ( header_ ) then 
        if ( my_id == t%root ) write(*,fmt_header) '', 'Min (secs.)', 'Max (secs.)', 'Avg (secs.)'
      end if         
  
      call mpi_allreduce ( accum_min, accum_min, 1, mpi_real8, mpi_min, t%mpi_comm, ierr  )
      call mpi_allreduce ( accum_max, accum_max, 1, mpi_real8, mpi_max, t%mpi_comm, ierr  )
      call mpi_allreduce ( accum_sum, accum_sum, 1, mpi_real8, mpi_sum, t%mpi_comm, ierr  )

      if ( my_id == t%root ) write(*,fmt_data) t%message, accum_min, accum_max, accum_sum/num_procs
    end subroutine timer_report
    
    subroutine timer_destroy (t)
      implicit none
      type(timer), intent(inout) :: t
      t%mpi_comm = mpi_comm_null
      if (allocated(t%message)) deallocate(t%message)
      t%root     = -1
      t%start    = 0.0
      t%stop     = 0.0
      t%accum    = 0.0
    end subroutine timer_destroy

end module timer_class
