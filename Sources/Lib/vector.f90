module vector_class
  use data_desc_class
#ifdef MPI_MOD
  use mpi
#endif
  implicit none
#ifdef MPI_H
  include 'mpif.h'
#endif
  
  type vector
     integer                  :: n      = -1      ! Number of vector entries
     real(8), allocatable     :: data(:)          ! Vector entries
     type(data_desc), pointer :: desc   => NULL() ! Pointer to the FE mesh descriptor
  end type vector

  integer, parameter :: MAX_TAG      = 10000
  integer            :: current_tag  = 0 
contains  
  subroutine vector_create ( desc, v )
    implicit none
    ! Parameters
    type(data_desc), target, intent(in)    :: desc
    type (vector)          , intent(inout) :: v
    call vector_destroy (v)
    ! Point to the descriptor of the distributed FE mesh
    v%desc => desc 
    v%n = v%desc%node_num
    allocate ( v%data(1:v%n) )
    v%data = 0.0
  end subroutine vector_create

  subroutine vector_axpby ( alpha, beta, x, y )
!*****************************************************************************
!
!  vector_axpby performs the vector update y = beta*y + alpha*x,
!  where beta and alpha are scalars; x and y are vectors.
!
!  Parameters:
!
!    Input, real ( kind = 8 ) alpha, beta, scalars involved in the
!    vector operator
!   
!    Input, type(vector) x. Input vector x.
!
!    Input/Output, type(vector) y. Updated vector y.
!
    implicit none
    ! Parameters
    real(8)     , intent(in)    :: alpha, beta
    type(vector), intent(in)    :: x
    type(vector), intent(inout) :: y
    y%data = beta * y%data + alpha * x%data
  end subroutine vector_axpby

  subroutine vector_copy ( x, y )
!*****************************************************************************
!
!  vector_copy copies x into y 
!
!  Parameters:
!
!    Input, type(vector) x. Input vector x.
!
!    Input/Output, type(vector) y. Vector y resulting from the copy.
!
    implicit none
    ! Parameters
    type(vector), intent(in)    :: x
    type(vector), intent(inout) :: y
    y%data = x%data
  end subroutine vector_copy

  subroutine vector_dot ( x, y, alpha )
    implicit none
    ! Parameters 
    type(vector), intent(in)  :: x
    type(vector), intent(in)  :: y
    real(8)     , intent(out) :: alpha
    
    ! Locals
    integer :: ierr

    ! *** Master on Numerical Methods in Engineering (MNME) ***
    ! *** Module Name: Domain Decomposition and Large Scale Scientific Computing (DDLSSC) ***
    ! *** TASK #1
    ! *** CODE DEVELOPMENT REQUIRED ***
    !     THE BODY OF THE FOLLOWING 
    !     SUBROUTINE MUST BE DEVELOPED
    !     FOR THE FINAL ASSIGNMENT

    if (x%desc%me == 0) then
       write (*,'(a)') '! *** Master on Numerical Methods in Engineering (MNME) ***'
       write (*,'(a)') '! *** Module Name: Domain Decomposition and Large Scale Scientific Computing (DDLSSC) ***'
       write (*,'(a)') '! *** TASK #1'
       write (*,'(a)') '! *** CODE DEVELOPMENT REQUIRED ***'
       write (*,'(a)') '!     THE BODY OF THE FOLLOWING'
       write (*,'(a)') '!     SUBROUTINE MUST BE DEVELOPED'
       write (*,'(a)') '!     FOR THE FINAL ASSIGNMENT'
    end if
    call mpi_barrier(x%desc%mpi_comm, ierr)
    call mpi_abort(x%desc%mpi_comm, -1, ierr)

  end subroutine vector_dot

  subroutine vector_nrm2 ( x, nrm2 )
    implicit none
    ! Parameters 
    type(vector), intent(in)  :: x
    real(8)     , intent(out) :: nrm2
    ! Locals
    integer              :: ierr

    ! *** Master on Numerical Methods in Engineering (MNME) ***
    ! *** Module Name: Domain Decomposition and Large Scale Scientific Computing (DDLSSC) ***
    ! *** TASK #3
    ! *** CODE DEVELOPMENT REQUIRED ***
    !     THE BODY OF THE FOLLOWING 
    !     SUBROUTINE MUST BE DEVELOPED
    !     FOR THE FINAL ASSIGNMENT
    if (x%desc%me == 0) then
       write (*,'(a)') '! *** Master on Numerical Methods in Engineering (MNME) ***'
       write (*,'(a)') '! *** Module Name: Domain Decomposition and Large Scale Scientific Computing (DDLSSC) ***'
       write (*,'(a)') '! *** TASK #3'
       write (*,'(a)') '! *** CODE DEVELOPMENT REQUIRED ***'
       write (*,'(a)') '!     THE BODY OF THE FOLLOWING'
       write (*,'(a)') '!     SUBROUTINE MUST BE DEVELOPED'
       write (*,'(a)') '!     FOR THE FINAL ASSIGNMENT'
    end if
    call mpi_barrier(x%desc%mpi_comm, ierr)
    call mpi_abort(x%desc%mpi_comm, -1, ierr)

  end subroutine vector_nrm2

  subroutine vector_comm ( x, y )
  !*****************************************************************************
!
!  vector_comm sums-up all subdomain contributions (partially 
!  summed entries) to vector entries of x shared among several 
!  subdomains, i.e., vector entries corresponding to FE mesh nodes
!  laying on the interface, and stores the sum into y. In the case 
!  we are facing (1D FE mesh data distribution), a given node on the 
!  interface is shared among at most 2 different subdomains.
!
!  Parameters:
!
!    Input, type(vector) x. Input (partially summed) vector x. 
!
!    Input/Output, type(vector) y. Output (fully summed) vector y.  
!
    implicit none
    ! Parameters 
    type(vector), intent(in)     :: x
    type(vector), intent(inout)  :: y
    
    ! Locals
    integer              :: ierr
    ! *** Master on Numerical Methods in Engineering (MNME) ***
    ! *** Module Name: Domain Decomposition and Large Scale Scientific Computing (DDLSSC) ***
    ! *** TASK #2
    ! *** CODE DEVELOPMENT REQUIRED ***
    !     THE BODY OF THE FOLLOWING 
    !     SUBROUTINE MUST BE DEVELOPED
    !     FOR THE FINAL ASSIGNMENT
    if (x%desc%me == 0) then
       write (*,'(a)') '! *** Master on Numerical Methods in Engineering (MNME) ***'
       write (*,'(a)') '! *** Module Name: Domain Decomposition and Large Scale Scientific Computing (DDLSSC) ***'
       write (*,'(a)') '! *** TASK #2'
       write (*,'(a)') '! *** CODE DEVELOPMENT REQUIRED ***'
       write (*,'(a)') '!     THE BODY OF THE FOLLOWING'
       write (*,'(a)') '!     SUBROUTINE MUST BE DEVELOPED'
       write (*,'(a)') '!     FOR THE FINAL ASSIGNMENT'
    end if
    call mpi_barrier(x%desc%mpi_comm, ierr)
    call mpi_abort(x%desc%mpi_comm, -1, ierr)
    
  end subroutine vector_comm


  subroutine vector_weight ( x )
!*****************************************************************************
!
!  vector_weight multiplies each component of x shared among
!  several subdomains (i.e., vector entries corresponding to FE 
!  mesh nodes laying on the interface) by the inverse of the 
!  number of subdomains (i.e., by 1/2) 
!
!  Parameters:
!
!    Input/Output, type(vector) x. Output vector x.  
!
    implicit none
    ! Parameters 
    type(vector), intent(inout)  :: x 
    
    ! Locals
    integer              :: ierr

    ! *** Master on Numerical Methods in Engineering (MNME) ***
    ! *** Module Name: Domain Decomposition and Large Scale Scientific Computing (DDLSSC) ***
    ! *** TASK #4
    ! *** CODE DEVELOPMENT REQUIRED ***
    !     THE BODY OF THE FOLLOWING 
    !     SUBROUTINE MUST BE DEVELOPED
    !     FOR THE FINAL ASSIGNMENT
    if (x%desc%me == 0) then
       write (*,'(a)') '! *** Master on Numerical Methods in Engineering (MNME) ***'
       write (*,'(a)') '! *** Module Name: Domain Decomposition and Large Scale Scientific Computing (DDLSSC) ***'
       write (*,'(a)') '! *** TASK #4'
       write (*,'(a)') '! *** CODE DEVELOPMENT REQUIRED ***'
       write (*,'(a)') '!     THE BODY OF THE FOLLOWING'
       write (*,'(a)') '!     SUBROUTINE MUST BE DEVELOPED'
       write (*,'(a)') '!     FOR THE FINAL ASSIGNMENT'
    end if
    call mpi_barrier(x%desc%mpi_comm, ierr)
    call mpi_abort(x%desc%mpi_comm, -1, ierr)

  end subroutine vector_weight


  subroutine vector_destroy ( v ) 
    implicit none
    ! Parameters
    type (vector), intent(inout)     :: v
    v%n = -1
    if (allocated (v%data)) deallocate ( v%data )
    nullify(v%desc)
  end subroutine vector_destroy
end module vector_class
