#################################################################
# TESTS
#################################################################

FILE(GLOB_RECURSE DRIVERS_SRC *.f90 *.F90)
SET(DRIVERS_SRC ${DRIVERS_SRC} PARENT_SCOPE)

#################################################################
# DRIVERS
#################################################################

FOREACH(TEST_SRC ${DRIVERS_SRC})
    GET_FILENAME_COMPONENT(EXE_NAME ${TEST_SRC} NAME_WE)
    ADD_EXECUTABLE(${EXE_NAME} ${TEST_SRC})
    TARGET_LINK_LIBRARIES(${EXE_NAME} ${PROJECT_NAME} ${MPI_Fortran_LIBRARIES} ${${PROJECT_NAME}_EXTERNAL_LIBRARIES})
ENDFOREACH()



