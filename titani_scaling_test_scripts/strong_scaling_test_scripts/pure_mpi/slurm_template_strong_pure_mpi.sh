#!/bin/sh
#SBATCH --partition=express
#SBATCH --job-name=MNME-DDM
#SBATCH --output=NXxNY_PROCS_PREC/NXxNY_PROCS_PREC.out
#SBATCH --error=NXxNY_PROCS_PREC/NXxNY_PROCS_PREC.err
#SBATCH --cpus-per-task=1
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=24
#SBATCH --time=00:30:00

cd LAUNCH_DIR

nodes=$(mpirun -np 48 hostname | sort | uniq)
if [ PROCS -eq 1 ] 
then
 num_nodes=1
else
 num_nodes=2
fi
let PPN=PROCS/$num_nodes

####Generate hostfiles######
i=1
while [ $i -le $num_nodes ]
do
  j=1
  while [ $j -le $PPN ] 
  do
    host=`echo $nodes | sed s/" "" "*/#/g | cut -f$i -d#`
    echo $host >> hostfile
    let j=j+1
  done
  let i=i+1
done
############################

#####Generate rankfile######
core_pus="0#12#1#13#2#14#3#15#4#16#5#17#6#18#7#19#8#20#9#21#10#22#11#23"
i=1
task=0
while [ $i -le $num_nodes ]
do
  j=1
  while [ $j -le $PPN ]
  do
    host=`echo $nodes | sed s/" "" "*/#/g | cut -f$i -d#`
    echo "rank $task=$host slot=$(echo $core_pus|cut -f$j -d#)" >> rankfile
    let task=task+1
    let j=j+1
  done
  let i=i+1
done
############################

export OMP_NUM_THREADS=1; export MKL_NUM_THREADS=1; export OMP_DYSPLAY_ENV=VERBOSE; 
command="mpirun -np PROCS --hostfile hostfile --rankfile rankfile --report-bindings PROGREXEC NX NY 0.0 1.0 0.0 1.0 PREC"
$command > stdout 2> stderr
